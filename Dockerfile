FROM python:3.10-bullseye

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY suite/robot_selenium_grid.robot .

CMD tail -f /dev/null