*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${url}               https://www.example.com
${remote_url}        http://localhost:4444/wd/hub
${browser}           Chrome

*** Test Cases ***
Example Test
    ${chrome_options}=    Create Chrome Options
    Open Browser    ${url}    ${browser}    remote_url=${remote_url}    options=${chrome_options}

*** Keywords ***
Create Chrome Options
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    --start-maximized
    Call Method    ${chrome_options}    add_argument    --disable-infobars
    [Return]    ${chrome_options}
