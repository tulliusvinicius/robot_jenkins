*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${url}               https://www.saucedemo.com/
${navegador}         googlechrome
${remote_url}        http://selenium-hub:4444/wd/hub
${input_username}    user-name
${input_password}    password
${valor_username}    standard_user
${valor_password}    secret_sauce
${login_button}      login-button



*** Keywords ***
Create Chrome Options
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    --start-maximized
    Call Method    ${chrome_options}    add_argument    --disable-infobars
    [Return]    ${chrome_options}


Abrir pagina saucedemo.com
    ${chrome_options}=    Create Chrome Options
    Open Browser    ${url}    ${navegador}    remote_url=${remote_url}    options=${chrome_options}

preencher dados de login
    Input Text              ${input_username}    ${valor_username}
    Input Password          ${input_password}    ${valor_password}
    Capture Page Screenshot

clicar no botão para submeter
    Click Button            ${login_button}
    Close Browser


*** Test Cases ***

cenário 01 - realizar login com sucesso
    Abrir pagina saucedemo.com
    preencher dados de login
    clicar no botão para submeter