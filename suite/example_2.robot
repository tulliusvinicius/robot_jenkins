*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${url}               https://www.saucedemo.com/
${navegador}         chrome
${remote_url}        http://localhost:4444/wd/hub
${input_username}    user-name
${input_password}    password
${valor_username}    standard_user
${valor_password}    secret_sauce
${login_button}      login-button



*** Keywords ***

Abrir pagina saucedemo.com
    Open Browser    ${url}    ${navegador}    remote_url=${remote_url}

preencher dados de login
    Input Text              ${input_username}    ${valor_username}
    Input Password          ${input_password}    ${valor_password}
    Capture Page Screenshot

clicar no botão para submeter
    Click Button            ${login_button}
    Close Browser


*** Test Cases ***

cenário 01 - realizar login com sucesso
    Abrir pagina saucedemo.com
    preencher dados de login
    clicar no botão para submeter